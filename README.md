            ЗАПУСК КОНТЕЙНЕРА DOCKER
```$xslt
docker build -t ubuntu16 .
```
-t и имя здесь используется для присваивания тэга образу. 
Для вывода всех возможных ключей введите

```$xslt
docker build —help
```
, а ***точка*** в конце означает что 
 Докерфайл находится в той же категории, из которой выполняется команда.

Далее запускаем нашу новую Ubuntu в контейнере!
```$xslt
docker run --name Ubuntu16-04 -t -i ubuntu16
```
```$xslt
docker run --name Ubuntu16-04 -t -i ubuntu16 --user root
docker run --name Ubuntu16-04 -t -i ubuntu16 --user develop
```


Ключ --name используется для присвоения простого имени контейнеру,
в противном случае это будет довольно длинная цифро-буквенная комбинация.

Ключ --user - уточняет с какого пользователя внутри контейнера авторизоваться.

После запуска контейнера для того, чтобы вернуться в систему хоста 
нажмите ***CTRL+P***, 
а затем ***CTRL+Q***.

docker команды
https://habr.com/ru/company/flant/blog/336654/
Запуск остановленного контейнера
```
docker start <container id>
```
Подключение к существующему контейнеру
```
docker attach <name container>

#или
docker exec -u <username> -it <name container> bash
```

version: 1.0.1
fixed bag with pip packages

uninstalled: python-virtualenv
installed: python3-venv

using:
python3 -m venv ./venv_dir

